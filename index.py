# coding: utf-8
import turtle		         #导入turtle库包
turtle.screensize(200, 150)      #设置画布大小
turtle.speed(2) #设置画笔速度为2
turtle.fillcolor("black")        #填充颜色
turtle.begin_fill()              #开始画，类似起笔
turtle.up()                      #提笔
count = 1                        #计时器，用于计录次数
while count <= 5:                #控制绘制次数
    turtle.forward(250)	         #画笔绘制的方向，向前移动指定的距离
    turtle.right(144)		 #向右转144度
    count += 1                   #循环绘制
    turtle.down()                #下笔
turtle.end_fill()                #完成填充图片的绘制
turtle.done()  